
// Mad Lib
// Austin Lennert

#include <iostream>
#include <conio.h>
#include <string>
#include <fstream>

using namespace std;

void printStory(string inputArrays[]);
void saveStory(string inputWords[], string filePath);


int main()
{
	// File path to save story to
	string filePath = "D:\\School\\2. C++ Programming\\Week 7\\AL.FileIO\\story.txt";

	const int numOfInputs = 12;

	string inputPrompts[numOfInputs] = {  // Prompts of type of words we want user to enter
		"adjective",
		"sport",
		"city",
		"person",
		"action verb",
		"vehicle",
		"place",
		"noun",
		"adjective",
		"food",
		"liquid",
		"adjective"
	};
	string inputWords[numOfInputs];  // Actual user response

	// Get user inputs
	for (int i = 0; i < numOfInputs; i++) {
		cout << "\nEnter a(n) " << inputPrompts[i] << ": ";
		getline(cin, inputWords[i]);
	}


	// Print story
	printStory(inputWords);

	// Ask if user wants to save their story
	string input;
	cout << "\n\nWould you like to save this story to a file? (y/n) ";
	cin >> input;

	// Save story if they want to
	if (input == "y" || input == "Y") {
		saveStory(inputWords, filePath);
		cout << "Story saved to " << filePath << "!\n";
	}



	cout << "\n\n\nPress any key to exit...";
	(void)_getch();
	return 0;
}

// Outputs story to provided stream
void storyToStream(ostream& strm, string inputWords[]) {
	strm << "One day my " << inputWords[0] << " friend and I decided to go to the " << inputWords[1] << " game in " << inputWords[2] << ".\n";  // Line 1
	strm << "We really wanted to see " << inputWords[3] << " play.\n";
	strm << "See we " << inputWords[4] << " in the " << inputWords[5] << " and headed down to the " << inputWords[6] << " and bought some " << inputWords[7] << ".\n";
	strm << "We watched the game and it was " << inputWords[8] << ".\n";
	strm << "We ate some " << inputWords[9] << " and drank some " << inputWords[10] << ".\n";
	strm << "We had a " << inputWords[11] << " time, and can't wait to go again.\n";
};

// Prints story to cout
void printStory(string inputWords[]) {
	cout << "\n\n\n";
	storyToStream(cout, inputWords);
}

// Saves story to file
void saveStory(string inputWords[], string filePath) {
	ofstream ofs(filePath);
	storyToStream(ofs, inputWords);
}